from typing import Union

import yaml


class ConfigError(ValueError):
    pass


class Settings:
    def __init__(self):
        with open('settings.yaml') as f:
            self.data = yaml.safe_load(f)

    def get_sentry_dsn(self) -> Union[None, str]:
        if 'sentry_dsn' not in self.data:
            return None

        return self.data['sentry_dsn']

    def get_token(self) -> str:
        if 'token' not in self.data:
            raise ConfigError('A token needs to be defined in settings.yaml!')

        return self.data['token']

    def get_prefix(self) -> str:
        if 'prefix' not in self.data:
            return '-'

        return self.data['prefix']

    def do_delete_invoking(self):
        if 'delete_invoking' not in self.data:
            return False

        return self.data['delete_invoking']

    def get_nsfw_channel_id(self) -> str:
        if 'nsfw_channel_id' not in self.data:
            raise ConfigError('A channel ID for the NSFW channel needs to be defined in settings.yaml!')

        return self.data['nsfw_channel_id']

    def get_disallow_unbound_in(self) -> [str]:
        if 'disallow_unbound_in' not in self.data:
            return []

        return self.data['disallow_unbound_in']

    def get_autodelete_pictures_in(self) -> [str]:
        if 'autodelete_pictures' not in self.data or 'in' not in self.data['autodelete_pictures']:
            return []

        return self.data['autodelete_pictures']['in']

    def get_autodelete_pictures_timeout(self) -> int:
        if 'autodelete_pictures' not in self.data or 'timeout' not in self.data['autodelete_pictures']:
            return 60

        return self.data['autodelete_pictures']['timeout'] * 60
