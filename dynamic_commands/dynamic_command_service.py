import os
from typing import Optional

import yaml

from dynamic_commands.dynamic_command import DynamicCommand


def _get_settings_for_command(base_command: str) -> {}:
    """
    Load the settings for a command
    :param base_command: The command's base name
    :return: The settings dictionary
    """
    settings_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), base_command, 'settings.yaml'
    )

    try:
        with open(settings_path, encoding='utf-8') as f:
            settings = yaml.safe_load(f)

            if 'replies' not in settings or settings['replies'] is None:
                settings.update({'replies' : {}})

            return settings

    except:
        return {'replies' : {'forDialogues': ['$from an $to:'], 'forMonologues': ['$from:']}}


def try_resolve_command(command: str) -> Optional[DynamicCommand]:
    """
    Resolves a command / an alias.
    :return: The execute function if the command is found. None if not.
    """
    settings = None

    for base_command, aliases in get_command_list().items():
        if command == base_command or command in aliases:
            command = base_command
            settings = _get_settings_for_command(command)
            break

    if settings is None:
        return None

    return DynamicCommand(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), command),
        settings['replies'].get('forDialogues', []),
        settings['replies'].get('forMonologues', []),
        settings.get('bindTo', [])
    )

def get_command_list() -> {str : [str]}:
    """
    Get a list of all available commands.
    :return: A Dictionary with base_command : aliases
    """
    commands = {}

    folder_list = [
        f.name \
        for f in os.scandir(os.path.dirname(os.path.abspath(__file__))) \
        if f.is_dir() and not f.name.startswith('_')
    ]

    for folder_name in folder_list:
        commands.update({
            folder_name : _get_settings_for_command(folder_name).get('aliases', [])
        })

    return commands

def get_help_texts(bot_prefix: str) -> [str]:
    """
    Gets a list of help texts for all dynamic commands.
    :param bot_prefix: The current prefix of the bot's commands
    :return: An array of unsorted but fully formatted help messages
    """
    help_texts = []

    for command, aliases in get_command_list().items():
        dynC = try_resolve_command(command)

        hasMonologue = len(dynC.monologue_replies) > 0
        hasDialogue = len(dynC.dialogue_replies) > 0

        hasBind = len(dynC.bound_to_channel_ids) > 0

        help_texts.append(
            (
                '      \N{SMALL ORANGE DIAMOND} %s%s%s%s' % (
                    '`{prefix}{cmd}`' * hasMonologue,
                    ' / ' * (hasMonologue and hasDialogue),
                    '`{prefix}{cmd} <@Mention(s)>`' * hasDialogue,
                    '  [limitiert]' * hasBind,
                )
            ).format(prefix=bot_prefix, cmd=command) \
            +\
            ('\n      Aliasse: `' + '`, `'.join(aliases) + '`') * (len(aliases) > 0)
        )

    return help_texts
