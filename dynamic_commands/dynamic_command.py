import asyncio
import logging
import os
import random
from string import Template

from discord import Message, HTTPException, NotFound

from bot import GifBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOTE, TOO_LEWD_EMOTE, DISAPPROVAL_EMOTE


class DynamicCommand:
    def __init__(self, full_path: str, dialogue_replies: [str], monologue_replies: [str], bound_to_channel_ids: [str]):
        self.full_path = full_path
        self.dialogue_replies = dialogue_replies
        self.monologue_replies = monologue_replies
        self.bound_to_channel_ids = bound_to_channel_ids

    async def execute(self, bot: GifBot, message: Message, command: str, args: [str]):
        await bot.send_typing(message.channel)

        await self._check_if_allowed(bot, message, command)

        reply_txt = await self._try_build_reply_txt(message)

        await bot.send_message(
            message.channel,
            reply_txt
        )

        successful = False
        picture_msg = None
        max_tries = len(self.get_file_list())  # should be a good approximation
        tries = 0
        while not successful:
            gif_path = await self._get_rnd_gif_path()

            logging.info('Sending gif "%s"...' % gif_path)
            try:
                picture_msg = await bot.send_file(message.channel, gif_path)
            except HTTPException as http_ex:
                error_code = getattr(http_ex, 'code', None)
                if error_code == 40005:  # = file too big
                    tries += 1
                    if tries < max_tries:
                        continue
                    else:
                        raise StyledUserNotification(
                            DISAPPROVAL_EMOTE +
                            'Leider kein verwendbares gif vorhanden.'
                        )
                elif 500 <= error_code < 600:
                    raise StyledUserNotification(
                        TABLE_FLIPPING_EMOTE +
                        ('Leider haben die Discord Server aktuell ein Problem (`%d`).\n' % error_code) +
                        'Bitte versuche es später erneut.'
                    )
                else:
                    raise http_ex

            successful = True

            try:
                await bot.delete_message(message)
            except NotFound:
                pass
            logging.info('...done')

        if picture_msg is not None and message.channel.id in bot.settings.get_autodelete_pictures_in():
            logging.info('Waiting to delete picture...')
            await asyncio.sleep(bot.settings.get_autodelete_pictures_timeout())

            logging.info('Deleting picture...')
            await bot.delete_message(picture_msg)
            logging.info('...done')


    async def _check_if_allowed(self, bot: GifBot, message: Message, command: str) -> None:
        if len(self.bound_to_channel_ids) > 0:
            if message.channel.id not in self.bound_to_channel_ids:
                raise StyledUserNotification(
                    TOO_LEWD_EMOTE + \
                    'Nicht hier, geh damit in <#' + '> / <#'.join(self.bound_to_channel_ids) + '>' + \
                    '(Falls der Channel für dich nicht sichtbar ist, wende dich bitte an einen Admin oder Moderator)'
                )

        disallow_unbound_in = bot.settings.get_disallow_unbound_in()
        if message.channel.id in disallow_unbound_in and message.channel.id not in self.bound_to_channel_ids:
            raise StyledUserNotification(
                TOO_LEWD_EMOTE + \
                'Der command `%s` ist leider nicht in diesem Channel erlaubt!' % command
            )

    async def _try_build_reply_txt(self, message: Message) -> str:
        from_mention_str = message.author.mention

        mentions = [m.mention for m in message.mentions if m.id != message.author.id]

        if len(mentions) > 0:
            if len(self.dialogue_replies) == 0:
                raise StyledUserNotification(TABLE_FLIPPING_EMOTE + '%s wie soll das bitte gehen?!' % from_mention_str)
            reply_txt_template = random.choice(self.dialogue_replies)

            to_mention_str = \
                ', '.join(mentions[:-1]) +\
                ' und ' * (len(mentions) > 1) +\
                mentions[-1]

        else:
            if len(self.monologue_replies) == 0:
                raise StyledUserNotification(TABLE_FLIPPING_EMOTE + '%s sei mal nicht so egoistisch!' % from_mention_str)
            reply_txt_template = random.choice(self.monologue_replies)

            to_mention_str = ''

        return Template(reply_txt_template).substitute(
            {'from': from_mention_str, 'to': to_mention_str}
        )

    async def _get_rnd_gif_path(self) -> str:
        gif_file_list = self.get_file_list()
        return random.choice(gif_file_list)

    def get_file_list(self) -> [str]:
        return [
            f.path
            for f in os.scandir(self.full_path)
            if     f.is_file()
               and f.name.split('.')[-1] in ['gif', 'GIF', 'png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG']
        ]
