import os
from textwrap import dedent

from discord import Message

from bot import GifBot
from dynamic_commands import dynamic_command_service


async def cmd_help(bot: GifBot, message: Message, command: str, args: [str]):
    """
    `{prefix}help`
    Zeigt eine Liste an verfügbaren Befehlen an.
    """
    if bot.user.id not in message.raw_mentions:
        return

    await bot.send_typing(message.channel)

    help_list = []

    import static_commands
    for attribute in dir(static_commands):
        if attribute.startswith('cmd_'):
            docstring = getattr(static_commands, attribute).__doc__
            if docstring is None:
                continue
            help_text = dedent(docstring.format(prefix=bot.settings.get_prefix()))
            help_text_lines = help_text.split('\n')[1:-1]

            help_list.append('\N{SMALL ORANGE DIAMOND} %s' % (
                '\n         '.join(help_text_lines)
            ))

    help_list.append('\N{SMALL ORANGE DIAMOND} Reactions:')

    dynamic_commands_help_list = dynamic_command_service.get_help_texts(bot.settings.get_prefix())
    dynamic_commands_help_list.sort()
    help_list += dynamic_commands_help_list

    await bot.send_message(
        message.channel,
        'Der Moment, wenn %s keine Ahnung hat:' % message.author.mention
    )

    await bot.send_file(
        message.channel,
        os.path.join(os.path.dirname(os.path.abspath(__file__)), 'help.gif')
    )

    help_txt = ''
    for line in help_list:
        if len(help_txt + '\n' + line) >= 2000:
            await bot.send_message(message.channel, help_txt)
            help_txt = line
        else:
            help_txt += '\n' + line

    await bot.send_message(message.channel, help_txt)
