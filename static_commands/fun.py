from discord import Message

from bot import GifBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOTE


async def cmd_cookie(bot: GifBot, message: Message, command: str, args: [str]):
    """
    `{prefix}cookie <@User>`
    Mach dem per Mention angegebenen User eine Freude.
    """
    if len(message.mentions) == 0:
        raise StyledUserNotification(TABLE_FLIPPING_EMOTE + 'Du musst User per Mention angeben!')

    for user in message.mentions:
        await bot.send_message(
            message.channel, 'N\'aawww %s, %s gibt dir einen :cookie: :3'%(user.mention, message.author.mention)
        )
