import random
import socket

import aiohttp
import async_timeout
from discord import Message

from bot import GifBot
from custom_exceptions import NothingFoundError, StyledUserNotification, TOO_LEWD_EMOTE, TABLE_FLIPPING_EMOTE
from static_commands import image_fallbacks


async def _get_random_image_url(address: str, jkey: str, fallback_urls: [str]=None, params=None, recursion_level=0) -> str:
    """
    Gets an image URL, using the specified random service.
    Tries recursively until an URL which links to an image in a format supported by Discord is found or the API tells it
      that there is nothing to find.
    :param address: URL of the API to use
    :param jkey: JSON-Key of the response in which the image url will be located
    :param fallback_urls: List of urls to randomly select one from if the API fails
    :return: Image URL as returned by the API
    """
    if (recursion_level > 10):
        raise RuntimeError('Fehler beim Abrufen! (Tried too long to get valid file type)')

    res = {}

    conn = aiohttp.TCPConnector(family=socket.AF_INET)  # = only use IPv4

    async with async_timeout.timeout(1000):
        async with aiohttp.ClientSession(connector=conn) as cs:
            async with cs.get(address, params=params) as r:
                if r.status != 200:
                    if fallback_urls is not None:
                        return random.choice(fallback_urls)
                    else:
                        raise RuntimeError('Fehler beim Abrufen! (%s)' % r.status)

                res = await r.json()

    if type(res) == list:
        if len(res) == 0:
            raise NothingFoundError()
        else:
            res = res[0]

    if jkey in res:
        url = res[jkey]
    else:
        raise RuntimeError('Fehler beim Abrufen! (Invalid response)')

    if url.split('.')[-1] not in ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF']:
        return await _get_random_image_url(address, jkey, fallback_urls, params, recursion_level + 1)
    else:
        return url


async def cmd_cat(bot: GifBot, message: Message, command: str, args: [str]):
    """
    `{prefix}cat`
    Gibt dir ein zufälliges Katzen-Bild.
    """
    await bot.send_typing(message.channel)
    await bot.send_message(
        message.channel,
        ':smiley_cat: für %s: %s'%(
            message.author.display_name,
            await _get_random_image_url('https://aws.random.cat/meow', 'file', fallback_urls=image_fallbacks.cats)
        )
    )

async def cmd_dog(bot: GifBot, message: Message, command: str, args: [str]):
    """
    `{prefix}dog`
    Gibt dir ein zufälliges Hunde-Bild.
    """
    await bot.send_typing(message.channel)
    await bot.send_message(
        message.channel,
        ':dog: für %s: %s'%(
            message.author.display_name,
            await _get_random_image_url('https://random.dog/woof.json', 'url', fallback_urls=image_fallbacks.dogs)
        )
    )

async def cmd_hentai(bot: GifBot, message: Message, command: str, args: [str]):
    nsfw_channel_id = bot.settings.get_nsfw_channel_id()
    if nsfw_channel_id not in [c.id for c in list(message.server.channels)]:
        return  # fail silently
    if message.channel.id != nsfw_channel_id:
        raise StyledUserNotification(
            TOO_LEWD_EMOTE +
            'Nicht hier, geh damit in <#%s>! ' % nsfw_channel_id +
            '(Falls der Channel für dich nicht sichtbar ist, wende dich bitte an einen Admin oder Moderator)'
        )

    if len(args) == 0:
        await bot.send_typing(message.channel)

        params = [
            ('random', True),
            ('limit', 1),
            ('tags', 'rating:explicit')
        ]

        post_url = await _get_random_image_url(
            'http://danbooru.donmai.us/posts.json', 'file_url', params=params
        )

        await bot.send_message(
            message.channel,
            ':HellsHentai: für %s: %s'%(
                message.author.display_name, post_url
            )
        )

    else:
        args = [a.lower() for a in args]

        if len({'loli', 'shota', 'toddlercon'}.intersection(args)) > 0:
            raise StyledUserNotification(
                TOO_LEWD_EMOTE +
                'Suchen nach `loli`, `shota` oder `toddlercon` werden nicht gedulded.'
            )

        await bot.send_typing(message.channel)

        try:
            params = [
                ('random', True),
                ('limit', 1),
                ('tags', 'rating:explicit+' + '_'.join(args)
                )
            ]

            post_url = await _get_random_image_url(
                'http://danbooru.donmai.us/posts.json', 'file_url', params=params
            )
        except NothingFoundError:
            raise StyledUserNotification(TABLE_FLIPPING_EMOTE + 'Keinen Post gefunden!')

        await bot.send_message(
            message.channel,
            ':HellsHentai: "%s" für %s: %s'%(
                ' '.join(args), message.author.display_name, post_url
            )
        )
