from discord import Message

from bot import GifBot
from dynamic_commands.dynamic_command_service import get_command_list, try_resolve_command


async def cmd_gifcount(bot: GifBot, message: Message, command: str, args: [str]):
    """
    `{prefix}gifcount`
    Zeigt an, wie viele Reaktions-Gifs verfügbar sind.
    """
    await bot.send_typing(message.channel)

    total_count = 0
    most_cmd = ''
    most_count = 0

    command_list = get_command_list()
    for cmd in command_list:
        count = len(try_resolve_command(cmd).get_file_list())
        total_count += count

        if count > most_count:
            most_cmd   = cmd
            most_count = count

    await bot.send_message(
        message.channel, 'Insgesamt sind **%s** gifs verfügbar.\nDie meisten hat `%s` mit _%s_ gifs.'%(
            total_count, most_cmd, most_count
        )
    )
