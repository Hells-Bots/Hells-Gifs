import logging

from bot import GifBot


bot = GifBot()
try:
    bot.startup()
except KeyboardInterrupt:
    try:
        logging.info('Got KeyboardInterrupt, kthxbai')
    except:
        pass
