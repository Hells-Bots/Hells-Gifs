TABLE_FLIPPING_EMOTE = '(╯\'\N{WHITE UP-POINTING SMALL TRIANGLE}\')╯︵ ┻━━┻  '
TOO_LEWD_EMOTE       = '(/ω＼)  '
DISAPPROVAL_EMOTE    = 'ಠ_ಠ  '


class StyledUserNotification(BaseException):
    """
    Notifies the user about something they did wrong.
    Not logged and not handled as an error.
    """
    pass


class NothingFoundError(BaseException):
    pass
