import asyncio
import logging
import os
import traceback

try:
    from raven import fetch_git_sha
    from raven.handlers.logging import Client as RavenClient
except ModuleNotFoundError:
    pass

from discord import Game, Client as DiscordClient, errors, Message, NotFound, Permissions

from custom_exceptions import StyledUserNotification
from settings_service import Settings


class GifBot(DiscordClient):
    def __init__(self, **options):
        super().__init__(**options)

        self.settings = Settings()

        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)

        f_handler = logging.FileHandler('run.log', 'w', encoding='UTF-8')
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(name)s | %(message)s')
        f_handler.setFormatter(formatter)
        root_logger.addHandler(f_handler)

        if self.settings.get_sentry_dsn() is not None:
            self.r_client = RavenClient(
                self.settings.get_sentry_dsn(),
                auto_log_stacks=True,
                release=fetch_git_sha(os.path.dirname(__file__))
            )
        else:
            self.r_client = None

    def startup(self):
        token = self.settings.get_token()

        try:
            self.loop.run_until_complete(self.start(token))
        except errors.LoginFailure:
            raise RuntimeError('Please fix the given Token-Value inside settings.yaml!')

    async def on_ready(self):
        await self.change_presence(
            game=Game(name="{}help".format(self.settings.get_prefix()))
        )


    async def on_message(self, message: Message):
        await self.wait_until_ready()

        if self.r_client:
            self.r_client.context.activate()
            self.r_client.context.merge({'tags': {
                'message_content': message.content,
                'discord_author_display_name': message.author.display_name,
                'discord_author_id': message.author.id,
                'discord_server_name': message.server.name if message.channel is not None else '[private]',
                'discord_server_id': message.server.id if message.channel is not None else 'n/a',
                'discord_channel_name': message.channel.name if message.channel is not None else 'n/a',
                'discord_channel_id': message.channel.id if message.channel is not None else 'n/a',
            }})
        try:
            # Preflight - Checks:
            if message.author == self.user:
                return

            content = message.content.strip()
            if not content.startswith(self.settings.get_prefix()):
                return

            # Unpacking:
            command, *args = content.split(self.settings.get_prefix(), 1)[-1].split(' ')

            if len(command) == 0:
                return

            if self.r_client:
                self.r_client.context.merge({'tags': {
                    'command': command
                }})

            # load handler
            try:
                import static_commands
                handler = getattr(static_commands, 'cmd_' + command, None)

                if handler is None:
                    from dynamic_commands.dynamic_command_service import try_resolve_command
                    handler = try_resolve_command(command)
                    if handler is not None:
                        handler = handler.execute
            except BaseException as ex:
                logging.error('Error while trying to load the command handlers: ' + str(ex))
                return

            if handler is None:
                return

            # Okay, let's do this!
            logging.info('got cmd: s: "%s" - c: "%s" - u: "%s %s" - m: "%s"' % (
                message.server.name if message.server is not None else '[private]',
                message.channel.name if message.server is not None else 'n/a',
                message.author.display_name,
                message.author.mention,
                content
            ))

            needed_permissions = Permissions(125952)
            bot_member = message.server.get_member(self.user.id)
            current_permissions = message.channel.permissions_for(bot_member)
            if not current_permissions.is_superset(needed_permissions):
                logging.info('cmd aborted, insufficient permissions')
                return

            try:
                await handler(self, message, command, args)
                logging.info('cmd done')

            except StyledUserNotification as sun:
                logging.info('cmd done, with User Notification "' + str(sun) + '"')

                reply = await self.send_message(
                    message.channel, str(sun)
                )
                if self.settings.do_delete_invoking() and message.server is not None:
                    await asyncio.sleep(17)
                    await self.delete_message(reply)

            except BaseException as ex:
                if self.r_client:
                    self.r_client.captureException(exec_info=True)
                logging.error(str(ex))
                logging.error(traceback.format_exc())

                reply = await self.send_message(
                    message.channel, '¯\\_(ツ)_/¯  ' + str(ex)
                )
                if self.settings.do_delete_invoking() and message.server is not None:
                    await asyncio.sleep(17)
                    await self.delete_message(reply)

            finally:
                if self.settings.do_delete_invoking() and message.server is not None:
                    try:
                        await self.delete_message(message)
                    except NotFound:
                        pass

        finally:
            if self.r_client:
                self.r_client.context.clear()
